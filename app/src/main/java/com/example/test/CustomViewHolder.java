package com.example.test;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class CustomViewHolder extends RecyclerView.ViewHolder {
    TextView textTile, textSource;
    ImageView imgHeadline;
    CardView cardView;

    public CustomViewHolder(@NonNull View itemView) {
        super(itemView);
        textTile = itemView.findViewById(R.id.newsTitle);
        textSource = itemView.findViewById(R.id.newsData);
        imgHeadline = itemView.findViewById(R.id.iv_match_user_pic);
        cardView = itemView.findViewById(R.id.containerNews);
    }
}
