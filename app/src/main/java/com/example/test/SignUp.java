package com.example.test;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignUp extends Fragment {

    View view;
    EditText nameNew, emailNew, passwordNew, numberNew;
    MainActivity mainActivity;
    String newName, newEmail, newPass;
    TextView logIn;
    FirebaseAuth mAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        SignUpHeader signUpHeader = new SignUpHeader();
        getParentFragmentManager().beginTransaction().replace(R.id.header, signUpHeader).commit();
        nameNew = view.findViewById(R.id.nameId);
        emailNew = view.findViewById(R.id.su_emailId);
        logIn = view.findViewById(R.id.bt_logIn);
        passwordNew = view.findViewById(R.id.su_password);
        numberNew = view.findViewById(R.id.number);
        mainActivity = (MainActivity) getActivity();
        mAuth = FirebaseAuth.getInstance();
        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignIn signIn = new SignIn();
                getParentFragmentManager().beginTransaction().replace(R.id.fragmentFrame, signIn).commit();
            }
        });
        mainActivity.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newName = nameNew.getText().toString();
                newEmail = emailNew.getText().toString();
                newPass = passwordNew.getText().toString();
                if (newEmail.isEmpty()||newPass.isEmpty()){
                    Toast.makeText(getContext(), "Fields can not be empty!", Toast.LENGTH_SHORT).show();
                }else{
                    mAuth.createUserWithEmailAndPassword(newEmail, newPass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(getContext(), "Account Created", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getContext(), HomeScreen.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

        return view;
    }
}