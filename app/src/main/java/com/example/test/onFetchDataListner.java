package com.example.test;

import java.util.List;

public interface onFetchDataListner<ApiResponse> {
    void onFetchData(List<News> list, String msg);
    void onError(String msg);

}
