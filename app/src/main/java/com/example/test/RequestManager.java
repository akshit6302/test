package com.example.test;

import android.content.Context;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class RequestManager {
    Context context;
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://newsapi.org/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public void getNewsHeadlines(onFetchDataListner listner, String query){
        callNewsApi  callNewsApi = retrofit.create(RequestManager.callNewsApi.class);
        Call<ApiResponse> call = callNewsApi.callHeadlines("in", query, "fe6856b2dcb14937bad0da1518757d0e");
        try {
            call.enqueue(new Callback<ApiResponse>() {
                @Override
                public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                    if (!response.isSuccessful()){
                        Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
                    }
                    listner.onFetchData(response.body().getArticles(), response.message());
                }

                @Override
                public void onFailure(Call<ApiResponse> call, Throwable t) {
                    listner.onError("Request Fail");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public RequestManager(Context context) {
        this.context = context;
    }
    public interface callNewsApi{
        @GET("top-headlines")
        Call<ApiResponse> callHeadlines(
                @Query("country") String country,
                @Query("q") String query,
                @Query("apiKey") String apiKey
        );

    }
}
