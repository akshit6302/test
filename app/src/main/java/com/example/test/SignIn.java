package com.example.test;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class SignIn extends Fragment {

    View view;
    int RC_SIGN_IN = 0;
    GoogleSignInClient mGoogleSignInClient;
    SignInButton button;
    TextView register;
    MainActivity mainActivity;

    EditText emailId, password1;
    FirebaseAuth mAUth;

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        button = view.findViewById(R.id.sign_in_button);
        register = view.findViewById(R.id.registerNow);
        LogInHeader logInHeader = new LogInHeader();
        getParentFragmentManager().beginTransaction().replace(R.id.header, logInHeader).commit();
        mainActivity = (MainActivity) getActivity();
        emailId= view.findViewById(R.id.emailId);
        password1 = view.findViewById(R.id.password);
        mAUth = FirebaseAuth.getInstance();
        mainActivity.button.setText("Sign In");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.sign_in_button:
                        signIn();
                        break;
                    // ...
                }


            }
        });


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignUp signUp = new SignUp();
                getParentFragmentManager().beginTransaction().replace(R.id.fragmentFrame, signUp).commit();

            }
        });
        mainActivity.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailId.getText().toString();
                String passwordCheck = password1.getText().toString();
                if (email.isEmpty()||passwordCheck.isEmpty()){
                    Toast.makeText(mainActivity, "Fields can not be empty!", Toast.LENGTH_SHORT).show();
                }else{
                    mAUth.signInWithEmailAndPassword(email,passwordCheck).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isComplete()){
                                Toast.makeText(getContext(), "Welcome Back", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getContext(), HomeScreen.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(getContext(), "Incorrect Credentials", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    }
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);


        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

    }

    private void handleSignInResult(Task<GoogleSignInAccount> task) {
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getActivity());
        Intent intent = new Intent(getContext(), HomeScreen.class);
        startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getActivity());
    }
}