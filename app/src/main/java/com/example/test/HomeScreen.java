package com.example.test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class HomeScreen extends AppCompatActivity {

    RecyclerView recyclerView;
    CustomAdapter adapter;
    EditText query;
    ImageButton btSearch;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        query = findViewById(R.id.bucket);
        button = findViewById(R.id.btLogOut);
        btSearch = findViewById(R.id.search);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                finish();
            }
        });
        btSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = query.getText().toString();
                if (!text.isEmpty()){
                    RequestManager requestManager = new RequestManager(HomeScreen.this);
                    requestManager.getNewsHeadlines(listner, text);
                }
                }
        });
        RequestManager requestManager = new RequestManager(this);
        requestManager.getNewsHeadlines(listner, null);
    }
    private final onFetchDataListner<ApiResponse> listner = new onFetchDataListner<ApiResponse>() {
        @Override
        public void onFetchData(List<News> list, String msg) {
            showNews(list);
        }

        @Override
        public void onError(String msg) {

        }
    };

    private void showNews(List<News> list) {
        recyclerView = findViewById(R.id.recylerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        adapter = new CustomAdapter(this, list);
        recyclerView.setAdapter(adapter);
    }


}